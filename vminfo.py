import socket
import platform

#hostname
hostname = socket.gethostname()

#ip address
ip = socket.gethostbyname(hostname)

#OS
os = platform.system()

#Relase of OS
release = platform.release()

#Version of OS
vs = platform.version()

#Machine arch
arch = platform.machine()


#printout everything 
print("Hostname: ", hostname)
print("IP Address: ", ip)
print("OS: ", os)
print("OS release: ", release)
print("OS Version: ", vs)
print("Architecture : ", arch)
